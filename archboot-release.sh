#!/bin/bash
. /etc/archboot/defaults
PKG="archboot archboot-bootloader archboot-qemu"
# create repository
[[ ! -d repository ]] && mkdir repository
# build packages
for i in ${PKG}; do
	cd ${i}
	updpkgsums PKGBUILD
	makepkg -c || exit 1
	mv *.zst ../repository
	rm -rf *.log 
	cd ..
done
# update db
cd repository
repo-add -q archboot.db.tar.gz $(find "./" -type f ! -name '*.sig' -name '*.zst') || exit 1
# server release
for i in *.zst; do gpg ${_GPG} $i; done
echo "Syncing files to ${_SERVER}..."
rsync -l -a --delete ./ "${_SERVER}":public_html/pkg/ 
cd ..
# cleanup
echo "Cleaning up..."
rm -rf repository
echo "Finished."
