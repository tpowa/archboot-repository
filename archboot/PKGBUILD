# Maintainer: Tobias Powalowski <tpowa@archlinux.org>

pkgbase=archboot
pkgname=('archboot' 'archboot-arm' 'archboot-riscv')
pkgver=202503010000
pkgrel=1
pkgdesc="Advanced, modular Arch Linux boot/install image creation utility"
arch=(any)
license=('GPL-3.0-or-later')
url="https://archboot.com"
depends=(
  3cpio
  arch-install-scripts
  archboot-bootloader
  bandwhich
  base
  bash
  bash-completion
  bat
  bcachefs-tools
  bottom
  btrfs-progs
  cdrtools
  choose
  cifs-utils
  cryptsetup
  curl
  dialog
  diffutils
  dog
  dosfstools
  dust
  dysk
  e2fsprogs
  efibootmgr
  efitools
  elinks
  ethtool
  exfatprogs
  eza
  fbset
  fd
  fzf
  gpm
  grml-zsh-config
  grub
  hdparm
  hwdetect
  iw
  iwd
  kexec-tools
  less
  libisoburn
  limine
  linux
  lshw
  lvm2
  lzop
  mdadm
  netscanner
  miniserve
  mokutil
  mtools
  nano
  ncurses
  neovim
  nfs-utils
  nss
  nvme-cli
  openssh
  ouch
  partclone
  polkit
  procs
  ripgrep
  rsync
  rustscan
  sbctl
  sbsigntools
  screen
  sd
  sdparm
  smartmontools
  systemd-ukify
  usbutils
  terminus-font
  tmux
  trippy
  ttyd
  wireless-regdb
  xfsprogs
  zoxide
)
makedepends=('git')
optdepends=(
  'linux-firmware: for firmware inclusion on image file'
  'linux-firmware-marvell: for marvell firmware support'
  'qemu-user-static: for aarch64/riscv64 image building on x86_64'
  'qemu-user-static-binfmt: for aarch64/riscv64 building on x86_64'
  'archboot-qemu: ovmf for secure boot and u-boot riscv64'
)
source=("git+https://gitlab.archlinux.org/tpowa/archboot.git#tag=${pkgver}?signed")
backup=(
  etc/archboot/defaults
  etc/archboot/x86_64.conf
  etc/archboot/aarch64.conf
  etc/archboot/riscv64.conf
  etc/archboot/x86_64-latest.conf
  etc/archboot/aarch64-latest.conf
  etc/archboot/x86_64-local.conf
  etc/archboot/aarch64-local.conf
  etc/archboot/server-update.conf
)
install=archboot.install
b2sums=('c5f326e827d3355b86ce6218a2e48b0176d71bda45c3029cb9e7eaf6ec7f2258c478d4186b3d08c71c95abf7c1f66c5f43433f9637604fd3e63aa5abb6e601b4')
validpgpkeys=(
  '5B7E3FB71B7F10329A1C03AB771DF6627EDF681F' # Tobias Powalowski <tpowa@archlinux.org>
)

package_archboot()
{
depends+=(
  amd-ucode
  archlinux-userland-fs-cmp
  cpupower
  dmidecode
  edk2-shell
  intel-ucode
  memtest86+
  memtest86+-efi
  refind
)
  cd "${srcdir}/archboot"
  cp -r etc/ usr/ "${pkgdir}/"
  mkdir -p "${pkgdir}"/etc/archboot/{run,ssh}
}

package_archboot-arm()
{
depends+=(
  amd-ucode
  archlinux-userland-fs-cmp
  cpupower
  dmidecode
)
  cd "${srcdir}/archboot"
  cp -r etc/ usr/ "${pkgdir}/"
  mkdir -p "${pkgdir}"/etc/archboot/{run,ssh}
}

package_archboot-riscv()
{
  cd "${srcdir}/archboot"
  cp -r etc/ usr/ "${pkgdir}/"
  mkdir -p "${pkgdir}"/etc/archboot/{run,ssh}
}
