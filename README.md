# Archboot-Repository
PKGBUILDS of the archboot project
https://gitlab.archlinux.org/tpowa/archboot

Repository is located here:
```plaintext
[archboot]
# United States 
Server = https://pkg.archboot.com
# Europe
Server = https://pkg.archboot.eu
# Asia
Server = https://pkg.archboot.net
```
